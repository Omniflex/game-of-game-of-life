//General events
document.onkeypress = update;
document.ondragover = function(evt) {
  mousex = evt.pageX;
  mousey = evt.pageY;
}
document.ondrop= function(evt) {
  mousex = evt.pageX;
  mousey = evt.pageY;
}

//Init variables
var sizex = 1200;
var sizey = 400;
var sizeSquare = 10;
var gridx = ~~(sizex/sizeSquare);
var gridy = ~~(sizey/sizeSquare);
var mousex = 0;
var mousey = 0;
var dragID = 0;
var dragx = 0;
var dragy = 0;
var pointx = 0;
var pointy = 0;

//init array and buffer array
var arr = new Array(gridx);
var buff = new Array(gridy);
for (idx=0; idx<gridx; idx+=1){
  arr[idx] = new Array(gridy).fill(0);
  buff[idx] = new Array(gridy).fill(0);
}
for (i=0; i<gridx; i++){
  arr[i][0] = -1;
  arr[i][0] = -1;
  arr[i][gridy-1] = -1;
  arr[i][gridy-1] = -1;
  buff[i][0] = -1;
  buff[i][0] = -1;
  buff[i][gridy-1] = -1;
  buff[i][gridy-1] = -1;
}
for (j=0; j<gridy; j++){
  arr[0][j] = -1;
  arr[0][j] = -1;
  arr[gridx-1][j] = -1;
  arr[gridx-1][j] = -1;
  buff[0][j] = -1;
  buff[0][j] = -1;
  buff[gridx-1][j] = -1;
  buff[gridx-1][j] = -1;
}

function point_it(event){
  //Return the coordinates of the mouse on the draggable element
  pos_x = event.offsetX?(event.offsetX):event.pageX-document.getElementById("grid").offsetLeft;
  pos_y = event.offsetY?(event.offsetY):event.pageY-document.getElementById("grid").offsetTop;
  return([pos_x, pos_y]);
}


function allowDrop(ev) {
  //Prevent the opening of a new tab when dropping the image
  ev.preventDefault();
}


function drag(ev, id, sx, sy) {
  //Set every variable needed to drop the image at the correct place
  pointx = ~~(point_it(ev)[0]/sizeSquare);
  pointy = ~~(point_it(ev)[1]/sizeSquare);

  dragID = id;
  dragx = sx;
  dragy = sy;
}


if (!String.prototype.contains) {
  //Avoid warnings in the console
  Object.defineProperty(String.prototype, 'contains', {
    value: function(search, start) {
      if (typeof start !== 'number') {
        start = 0
      }

      if (start + search.length > this.length) {
        return false
      } else {
        return this.indexOf(search, start) !== -1
      }
    }
  })
}


function drop(ev) {
  //Create the correct pattern after dropping the image
  ev.preventDefault();
  var x = ~~(mousex/sizeSquare)-pointx;
  var y = ~~(mousey/sizeSquare)-pointy;

  if (dragID == 1 && x!=0 && y!= 0 && x < (gridx-dragx) && y < (gridy-dragy)){
    arr[x+1][y] = 1;
    arr[x+2][y+1] = 1;
    arr[x][y+2] = 1;
    arr[x+1][y+2] = 1;
    arr[x+2][y+2] =  1;

    buff[x+1][y] = 1;
    buff[x+2][y+1] = 1;
    buff[x][y+2] = 1;
    buff[x+1][y+2] = 1;
    buff[x+2][y+2] =  1;
    draw();
  }
}


function sumNear(i,j){
  //Compute the number of living cells around the one passed in argument
  var s = 0;
  for (p=-1; p<=1; p++){
    for(q=-1; q<=1; q++){
      if (arr[i+p][j+q] == 1 && (p!= 0 || q!=0)){
        s++;
      }
    }
  }
  return s;
}


function update(){
  //Update the grid, only checks around living cells to gain time
  var toCheck = [];
  for (i=1; i<gridx; i++){
    for (j=1; j<gridy; j++){

       if (arr[i][j] == 1){


         for (p=-1; p<=1; p++){
           for(q=-1; q<=1; q++){
             var isHere = false;

             var idx = 0;
             while (isHere == false && idx < toCheck.length){
               if ([i+p,j+q].equals(toCheck[idx])){
                 isHere = true;
                 break;
               }
              idx +=1;
           }
           if (!isHere && i+p != 0 && j+q !=0 && i+p != gridx-1 && j+q != gridy-1){
             toCheck.push([i+p,j+q]);
           }
         }
       }
     }
   }
 }


  for (i=0; i<toCheck.length; i++){
    var x = toCheck[i][0];
    var y = toCheck[i][1];
      var sum = sumNear(x,y);
      if ( !(sum==2 || sum==3) && arr[x][y] == 1){
        buff[x][y] = 0;
      }
      else if (arr[x][y]==0 && sum==3) {
        buff[x][y] = 1;
      }
   }
   for (i=0; i<gridx; i++){
     for (j=0; j<gridy; j++){
       arr[i][j] = buff[i][j];
     }
   }
   draw();
}


function draw() {
  //Draw the board by reading arr
  var canvas = document.getElementById("grid");
  if (canvas.getContext) {
    var ctx = canvas.getContext("2d");
    for (abs=0; abs <gridx; abs++){
      for(ord=0; ord<gridy; ord++){
        if (arr[abs][ord] == 0){
          ctx.fillStyle = "gray";
        }
        else if (arr[abs][ord] == -1){
          ctx.fillStyle = "black";
        }
        else {
          ctx.fillStyle = "white";
        }
        ctx.fillRect(abs*sizeSquare, ord*sizeSquare, sizeSquare, sizeSquare);
        ctx.strokeStyle = "black";
        ctx.strokeRect(abs*sizeSquare, ord*sizeSquare, sizeSquare, sizeSquare);
      }
    }
  }
}


function handleMouseClick(event) {
  var mousex = event.pageX;
  var mousey = event.pageY;
  var anchorx = ~~(mousex/sizeSquare);
  var anchory = ~~(mousey/sizeSquare);
  var canvas = document.getElementById("grid");
    if (arr[anchorx][anchory] == 1){
      arr[anchorx][anchory] = 0;
      buff[anchorx][anchory] = 0;
    }
    else if (arr[anchorx][anchory] == 0){
      arr[anchorx][anchory] = 1;
      buff[anchorx][anchory] = 1;
    }
    draw();
}

  // Warn if overriding existing method
  if(Array.prototype.equals)
      console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
  // attach the .equals method to Array's prototype to call it on any array
  Array.prototype.equals = function (array) {
      // if the other array is a falsy value, return
      if (!array)
          return false;

      // compare lengths - can save a lot of time
      if (this.length != array.length)
          return false;

      for (var i = 0, l=this.length; i < l; i++) {
          // Check if we have nested arrays
          if (this[i] instanceof Array && array[i] instanceof Array) {
              // recurse into the nested arrays
              if (!this[i].equals(array[i]))
                  return false;
          }
          else if (this[i] != array[i]) {
              // Warning - two different object instances will never be equal: {x:20} != {x:20}
              return false;
          }
      }
      return true;
  }
  // Hide method from for-in loops
  Object.defineProperty(Array.prototype, "equals", {enumerable: false});
