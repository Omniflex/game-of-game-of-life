var express = require('express');
var favicon = require('serve-favicon');
var path = require('path');


const VIEWS = path.join(__dirname, 'views');
const PUBLIC = path.join(__dirname, 'public');

var port = process.env.PORT || 3000
var host = process.env.HOST || '0.0.0.0'

app = express();
app.set('port', port);
app.set('host', host);
app.use(express.static(PUBLIC));
app.use(favicon('public/favicon.ico'));
app.set('views', VIEWS);
app.set('view engine', 'html');

app.get('/', function (req, res) {
  res.sendFile('page.html', { root : VIEWS });
});

app.listen(port, host, function() {
    console.log('Listening to port:  ' + port);
    console.log('At adress:  ' + host);

});
