
  //events definition
  document.onmousemove = getMousePos;
  document.onclick   = handleMouseClick;
  document.onkeypress = update;

  //size variables
  var sizex = 1200;
  var sizey = 400;
  var sizeSquare = 10;
  var gridx = ~~(sizex/sizeSquare);
  var gridy = ~~(sizey/sizeSquare);
  var mousex = 0;
  var mousey = 0;
  var dragID = 0;
  var dragx = 0;
  var dragy = 0;
  var pointx = 0;
  var pointy = 0;

  //init arrays
  var arr = new Array(gridx);
  var buff = new Array(gridy);

  for (idx=0; idx<gridx; idx+=1){
    arr[idx] = new Array(gridy).fill(0);
    buff[idx] = new Array(gridy).fill(0);
  }

  for (i=0; i<gridx; i++){
    arr[i][0] = -1;
    arr[i][0] = -1;
    arr[i][gridy-1] = -1;
    arr[i][gridy-1] = -1;
    buff[i][0] = -1;
    buff[i][0] = -1;
    buff[i][gridy-1] = -1;
    buff[i][gridy-1] = -1;
  }

  for (j=0; j<gridy; j++){
    arr[0][j] = -1;
    arr[0][j] = -1;
    arr[gridx-1][j] = -1;
    arr[gridx-1][j] = -1;
    buff[0][j] = -1;
    buff[0][j] = -1;
    buff[gridx-1][j] = -1;
    buff[gridx-1][j] = -1;
  }

  if (!String.prototype.contains) {
    Object.defineProperty(String.prototype, 'contains', {
      value: function(search, start) {
        if (typeof start !== 'number') {
          start = 0
        }

        if (start + search.length > this.length) {
          return false
        } else {
          return this.indexOf(search, start) !== -1
        }
      }
    })
  }


//toCheck.forEach(function(i){console.log(i)});
//console.log("\n\n");


    for (i=0; i<toCheck.length; i++){
      var x = toCheck[i][0];
      var y = toCheck[i][1];
        var sum = sumNear(x,y);
        if ( !(sum==2 || sum==3) && arr[x][y] == 1){
          buff[x][y] = 0;
        }
        else if (arr[x][y]==0 && sum==3) {
          buff[x][y] = 1;
        }
     }
     for (i=0; i<gridx; i++){
       for (j=0; j<gridy; j++){
         arr[i][j] = buff[i][j];
       }
     }
     draw();
  }

    function draw() {
      var canvas = document.getElementById("grid");
      if (canvas.getContext) {
        var ctx = canvas.getContext("2d");
        for (abs=0; abs <gridx; abs++){
          for(ord=0; ord<gridy; ord++){
            if (arr[abs][ord] == 0){
              ctx.fillStyle = "gray";
            }
            else if (arr[abs][ord] == -1){
              ctx.fillStyle = "black";
            }
            else {
              ctx.fillStyle = "white";
            }
            ctx.fillRect(abs*sizeSquare, ord*sizeSquare, sizeSquare, sizeSquare);
            ctx.strokeStyle = "black";
            ctx.strokeRect(abs*sizeSquare, ord*sizeSquare, sizeSquare, sizeSquare);
          }
        }
        document.getElementById("myLink").innerHTML=sizeSquare;
      }
    }






    // Warn if overriding existing method
    if(Array.prototype.equals)
        console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
    // attach the .equals method to Array's prototype to call it on any array
    Array.prototype.equals = function (array) {
        // if the other array is a falsy value, return
        if (!array)
            return false;

        // compare lengths - can save a lot of time
        if (this.length != array.length)
            return false;

        for (var i = 0, l=this.length; i < l; i++) {
            // Check if we have nested arrays
            if (this[i] instanceof Array && array[i] instanceof Array) {
                // recurse into the nested arrays
                if (!this[i].equals(array[i]))
                    return false;
            }
            else if (this[i] != array[i]) {
                // Warning - two different object instances will never be equal: {x:20} != {x:20}
                return false;
            }
        }
        return true;
    }

    // Hide method from for-in loops
    Object.defineProperty(Array.prototype, "equals", {enumerable: false});
